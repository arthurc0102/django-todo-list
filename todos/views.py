from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages as m

from core.forms import DeleteCheckForm

from .models import Todo
from .forms import TodoForm, TodoEditForm


def index(request):
    todos = Todo.objects.all()
    return render(request, 'todos/index.html', dict(todos=todos))


def new(request):
    form = TodoForm(request.POST or None)
    if form.is_valid():
        form.save()
        m.success(request, '新增成功！')
        return redirect('todos:index')

    return render(request, 'todos/new.html', dict(form=form))


def show(request, pk):
    todo = get_object_or_404(Todo, pk=pk)
    return render(request, 'todos/show.html', dict(todo=todo))


def edit(request, pk):
    todo = get_object_or_404(Todo, pk=pk)
    form = TodoEditForm(request.POST or None, instance=todo)
    if form.is_valid():
        form.save()
        m.success(request, '更新成功！')
        return redirect('todos:index')

    return render(request, 'todos/edit.html', dict(form=form))


def delete(request, pk):
    form = DeleteCheckForm(request.POST or None)
    if form.is_valid() and form.cleaned_data['checked']:
        print(form.cleaned_data['checked'])
        todo = get_object_or_404(Todo, pk=pk)
        todo.delete()
        m.success(request, '刪除成功！')
        return redirect('todos:index')

    return render(request, 'todos/delete.html', dict(form=form))
