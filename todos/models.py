from django.db import models


class Todo(models.Model):
    title = models.CharField('標題', max_length=150)
    content = models.TextField('內容')
    finish = models.BooleanField('已完成', default=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.title
