from django.forms import Form, BooleanField


class DeleteCheckForm(Form):
    checked = BooleanField(label='確定要刪除嗎？')
